<div class="tabs cart-tabs">
    <div class="tabs-headers">
        <div class="tabs-header active" data="1">
            <span><?php echo __('Dranken', 'hestia'); ?></span>
        </div>
        <div class="tabs-header" data="2">
            <span><?php echo __('Desert', 'hestia'); ?></span>
        </div>
        <div class="tabs-header" data="3">
            <span><?php echo __('Option', 'hestia'); ?></span>
        </div>
    </div>
    <div class="tabs-blocks">
        <div class="tabs-block active" data="1">
            <div class="block-content">
                <?php echo get_elementor_template(4815); ?>
            </div>
        </div>
        <div class="tabs-block" data="2">
            <div class="block-content">
                <?php echo get_elementor_template(4844); ?>
            </div>
        </div>
        <div class="tabs-block" data="3">
            <div class="block-content">
                <?php echo get_elementor_template(4847); ?>
            </div>
        </div>
    </div>
</div>
